package com.swurv.app;

import java.util.ArrayList;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;

public class SendEmails extends AsyncTask<String, Void, Void> {

	private Context m_context = null;
	private ArrayList<Location> m_locations = new ArrayList<Location>();
	private ArrayList<String> m_emails = new ArrayList<String>();
	private int m_frequency = 2; // Min
	String subject="", body="";

	public SendEmails(Context ctx, String sub, String b) {
		m_context = ctx;
		subject = sub;
		body = b;
	}

	@Override
	protected void onPreExecute() {
	}

	@Override
	protected Void doInBackground(String... params) {
		// Create google Map URL for route
		// http://maps.google.com/maps?saddr=st.%20louis,mo&daddr=washington,dc%20to:chicago,il%20to:new%20york,ny
		StringBuilder sb = new StringBuilder();
		// sb.append(info.Name +
		// " may be in need of emergency help as they have activated a panic beacon on their mobile device using PANIKhunters. Here's all their details: <br /><br />");
		// sb.append("Name: " + info.Name + "<br />");
		// sb.append("You can follow location updates of "+info.Name+" here: ");
		// sb.append(Const.PANIK_PAGE_URL + info.UniqueId + "<br />");
		// sb.append("Note: Location is updated to this link every "+
		// info.TransmissionFrequency +" mins");
		// sb.append("<br /><br /><br />");
		// //sb.append("<br style=\"background-image: url(\"http://panikhunters.com/ph/Images/ttext.png\"); height: 100px; width: 283px; background-repeat: no-repeat;\">PANIKHunters</br>");
		// sb.append("<img src=\"http://panikhunters.com/Images/ttext.png\" alt=\"PANIKHunters\"></img>");
		// sb.append("<br /><br />http://panikhunters.com");
		sb.append("Someone tried to unlock your phone");


		Mail mail = new Mail("dsboomer21@gmail.com", "adminteam");

		// Mail mail = new Mail("noreply@panikhunters.com", "phn0r3plY");
		ArrayList<String> toemails = new ArrayList<String>();
		toemails.add(params[0]);
		mail.addToEmails(toemails);
		// mail.addBCCEmails(m_emails);
		mail.setSubject(subject);
		// String mailbody =
		// Html.fromHtml(sb.toString()).toString().replace("####",
		// TextUtils.htmlEncode("<br style=\"background-image: url(\"http://panikhunters.com/ph/Images/ttext.png\"); height: 100px; width: 283px; background-repeat: no-repeat;\">PANIKHunters</br>"));
		mail.setBody(body);
		// Utils.LogInfo(mailbody);

		try {
			if (params[1].length() > 0)
				mail.addAttachment(params[1]);
			
			mail.send();
		} catch (Exception e) {
			e.printStackTrace();
		}
		publishProgress();
		try {
			Thread.sleep(m_frequency * 60 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	protected void onProgressUpdate(Void... values) {
		super.onProgressUpdate(values);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
	}
}
