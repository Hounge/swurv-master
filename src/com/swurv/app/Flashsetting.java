package com.swurv.app;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.SeekBar;


/**
 * Created by hounge on 8/2/14.
 */
public class Flashsetting extends BaseActivity {

    private SeekBar Contrast = null;
    private SeekBar Brightness = null;
    SharedPreferences settings;
    SharedPreferences.Editor editor;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flashsettings);
        settings = PreferenceManager.getDefaultSharedPreferences(this);
        editor = settings.edit();

        Contrast = (SeekBar) findViewById(R.id.volume_bar);

        Contrast.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangeda = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                progressChangeda = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {

                editor.putInt("progressChangeda", progressChangeda);
                editor.commit();
            }
        });

        Brightness = (SeekBar) findViewById(R.id.BrightnessBar);

       Brightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedb = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                progressChangedb = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                editor.putInt("progressChangedb", progressChangedb);
                editor.commit();
            }
        });


    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        SharedPreferences settings = getSharedPreferences("settings", Activity.MODE_PRIVATE);

        settings = PreferenceManager.getDefaultSharedPreferences(this);
        editor = settings.edit();

        Contrast = (SeekBar) findViewById(R.id.volume_bar);

        Contrast.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangeda = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                progressChangeda = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {

                editor.putInt("progressChangeda", progressChangeda);
                editor.commit();
            }
        });

        Brightness = (SeekBar) findViewById(R.id.BrightnessBar);

        Brightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChangedb = 0;

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                progressChangedb = progress;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                editor.putInt("progressChangedb", progressChangedb);
                editor.commit();
            }
        });


    }
}



