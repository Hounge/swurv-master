package com.swurv.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ToggleButton;


public class SettingsActivity extends BaseActivity {
    ToggleButton tgbAlarm,tgbEmail,tgbFlash,tgbPicture;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        tgbAlarm = (ToggleButton) findViewById(R.id.tgbAlarm);
        tgbEmail = (ToggleButton) findViewById(R.id.tgbEmail);
        tgbPicture = (ToggleButton) findViewById(R.id.tgbPicture);
        tgbFlash = (ToggleButton) findViewById(R.id.tgbFlash);

        boolean alarm = settings.getBoolean("alarm", true);
        if (alarm)
            tgbAlarm.setChecked(true);
        else
            tgbAlarm.setChecked(false);

        boolean em = settings.getBoolean("sendemail", true);
        if (em)
            tgbEmail.setChecked(true);
        else
            tgbEmail.setChecked(false);

        boolean pic = settings.getBoolean("picture", true);
        if (pic)
            tgbPicture.setChecked(true);
        else
            tgbPicture.setChecked(false);
        boolean flash = settings.getBoolean("flash", true);
        if (flash)
            tgbPicture.setChecked(true);
        else
            tgbPicture.setChecked(false);


        Button btn1 = (Button) findViewById(R.id.closebtn);
        btn1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();

            }
        });

        Button btn2 = (Button) findViewById(R.id.flash_settings);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                flashsettings(v);

            }


        });
    }
    public void flashsettings(View v) {
        Intent intent = new Intent(SettingsActivity.this, Flashsetting.class);
        startActivity(intent);

    }

    public void BackClick(View view) {
        finish();
    }

    public void tgAlarmClick(View view) {
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
            editor.putBoolean("alarm", true);
        } else {
            editor.putBoolean("alarm", false);
        }
        editor.commit();
    }

    public void tgEmailClick(View view) {
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
            editor.putBoolean("sendemail", true);
        } else {
            editor.putBoolean("sendemail", false);
        }
        editor.commit();
    }

    public void tgPictureClick(View view) {
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
            editor.putBoolean("picture", true);
        } else {
            editor.putBoolean("picture", false);
        }
        editor.commit();
    }

    public void tgFlashClick(View view) {
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
            editor.putBoolean("flash", true);
        } else {
            editor.putBoolean("flash", false);
        }
        editor.commit();


    }

}