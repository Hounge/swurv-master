package com.swurv.app;

import android.app.Service;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.IBinder;
import android.util.Log;


/**
 * Created by hounge on 7/12/14.
 */
public class PictureService extends Service {
    private static final String TAG = "PhotoCaptureService";

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        Log.d(TAG, "Starting the PhotoCaptureService");
        takePhoto();
    }

    private void takePhoto() {

        Log.d(TAG, "Preparing to take photo");
        Camera camera = null;

        try {

            camera = Camera.open();

        } catch (RuntimeException e) {

            Log.e(TAG, "Camera not available", e);
            return;
        }

        if (null == camera) {

            Log.e(TAG, "Could not get camera instance");
            return;
        }

        Log.d(TAG, "Got the camera, creating the dummy surface texture");
        SurfaceTexture dummySurfaceTexture = new SurfaceTexture(0);

        try {

            camera.setPreviewTexture(dummySurfaceTexture);

        } catch (Exception e) {

            Log.e(TAG, "Could not set the surface preview texture", e);
        }

        Log.d(TAG, "Preview texture set, starting preview");

        camera.startPreview();

        Log.d(TAG, "Preview started");

        camera.takePicture(null, null, new Camera.PictureCallback() {

            @Override
            public void onPictureTaken(byte[] data, Camera camera) {

                Log.d(TAG, "Photo taken, stopping preview");

                camera.stopPreview();

                Log.d(TAG, "Preview stopped, releasing camera");

                camera.release();

                Log.d(TAG, "Camera released");
            }
        });
    }
    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }
}