package com.swurv.app;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;

/**
 * Created by hounge on 7/12/14.
 */
public class AlarmPlayer extends Service {
    private static final String TAG = "AlarmPlayer";
    MediaPlayer mPlayer;
    SharedPreferences settings;


    public IBinder onBind(Intent intent) {
        return null;
    }


    public void onCreate() {


        mPlayer = MediaPlayer.create(
                AlarmPlayer.this, R.raw.alarm);
        mPlayer.start();

        stopSelf();
    }

    }