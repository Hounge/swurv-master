package com.swurv.app;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Looper;
import android.os.Process;
import android.util.Log;

public class SendMailsService extends Service implements Runnable {
	SendEmails emails = null;
	@Override
	public void onCreate() {
		
		super.onCreate();
		
	}
	
	@Override
	public void onStart(Intent intent, int startId) {
		Thread t = new Thread(null, this, "SendMailsService_Service");
		t.start();

		super.onStart(intent, startId);
	}
	
	@Override
	public void onDestroy() {
		if(emails != null){
			try {
				emails.cancel(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		super.onDestroy();
	}
	
	@Override
	public void run() {
		Log.d("SWURV", "Doing some work, look at me!");

		// Normally we would do some work here... for our sample, we will
		// just sleep for 4 seconds.
		Looper.prepare();
		try {
			emails = new SendEmails(getApplicationContext(),"","");
			emails.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	private final ISendMailsService.Stub mBinder = new ISendMailsService.Stub() {
		public int getPid() {
			return Process.myPid();
		}
	};
}
