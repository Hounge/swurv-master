package com.swurv.app;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Images.Media;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;



public class CaptureImageActivity extends BaseActivity  {
    private SurfaceView preview = null;
    private SurfaceHolder previewHolder = null;
    private Camera camera = null;
    private boolean inPreview = false;
    private boolean cameraConfigured = false;
    Camera.CameraInfo info = new Camera.CameraInfo();
    int camera_id = 0;
    SharedPreferences settings;
    SharedPreferences.Editor editor;

    // https://github.com/aporter/coursera-android/blob/master/Examples/AudioVideoCamera/src/course/examples/AudioVideo/Camera/AudioVideoCameraActivity.java
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        settings = PreferenceManager.getDefaultSharedPreferences(this);
        editor = settings.edit();

        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.capture_image);

        preview = (SurfaceView) findViewById(R.id.preview);
        previewHolder = preview.getHolder();
        previewHolder.addCallback(surfaceCallback);
        previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

    }

    @Override
    public void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {

            for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
                Camera.getCameraInfo(i, info);
                camera_id = i;
                if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    camera = Camera.open(i);
                }
            }
        }

        if (camera == null) {
            camera = Camera.open();
        }

        startPreview();
    }

    @Override
    public void onPause() {
        if (inPreview) {
            camera.stopPreview();
        }

        camera.release();
        camera = null;
        inPreview = false;

        super.onPause();
    }

    private Camera.Size getBestPreviewSize(int width, int height,
                                           Camera.Parameters parameters) {
        Camera.Size result = null;

        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size;
                } else {
                    int resultArea = result.width * result.height;
                    int newArea = size.width * size.height;

                    if (newArea > resultArea) {
                        result = size;
                    }
                }
            }
        }

        return (result);
    }

    private Camera.Size getSmallestPictureSize(Camera.Parameters parameters) {
        Camera.Size result = null;

        for (Camera.Size size : parameters.getSupportedPictureSizes()) {
            if (result == null) {
                result = size;
            } else {
                int resultArea = result.width * result.height;
                int newArea = size.width * size.height;

                if (newArea < resultArea) {
                    result = size;
                }
            }
        }

        return (result);
    }

    private void initPreview(int width, int height) {
        if (camera != null && previewHolder.getSurface() != null) {

            if (!cameraConfigured) {
                Camera.Parameters parameters = camera.getParameters();
                Camera.Size size = getBestPreviewSize(width, height, parameters);
                Camera.Size pictureSize = getSmallestPictureSize(parameters);

                if (size != null && pictureSize != null) {
                    parameters.setPreviewSize(size.width, size.height);
                    parameters.setPictureSize(pictureSize.width,
                            pictureSize.height);

                    int orientation = getWindowManager().getDefaultDisplay()
                            .getRotation();
                    int degrees = 0;
                    switch (orientation) {
                        case Surface.ROTATION_0:
                            degrees = 0;
                            break;
                        case Surface.ROTATION_90:
                            degrees = 90;
                            break;
                        case Surface.ROTATION_180:
                            degrees = 180;
                            break;
                        case Surface.ROTATION_270:
                            degrees = 270;
                            break;
                    }
                    // orientation = (orientation + 45) / 90 * 90;
                    int rotation = 0;

                    rotation = (info.orientation + degrees) % 360;
                    rotation = (360 - rotation) % 360;
                    camera.setDisplayOrientation(rotation);
                    parameters.setPictureFormat(ImageFormat.JPEG);
                    camera.setParameters(parameters);
                    cameraConfigured = true;
                }
            }

            try {
                camera.setPreviewDisplay(previewHolder);
            } catch (Throwable t) {
                Log.e("PreviewDemo-surfaceCallback",
                        "Exception in setPreviewDisplay()", t);
                Toast.makeText(CaptureImageActivity.this, t.getMessage(),
                        Toast.LENGTH_LONG).show();
            }
        }
    }



    private void startPreview() {
        if (cameraConfigured && camera != null) {
            camera.startPreview();
            inPreview = true;
        }
        if (inPreview) {
            camera.takePicture(null, null, photoCallback);
            inPreview = false;
        }
    }

    SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {
        public void surfaceCreated(SurfaceHolder holder) {
            // no-op -- wait until surfaceChanged()

            try {
                // This case can actually happen if the user opens and closes
                // the camera too frequently.
                // The problem is that we cannot really prevent this from
                // happening as the user can easily
                // get into a chain of activites and tries to escape using the
                // back button.
                // The most sensible solution would be to quit the entire
                // EPostcard flow once the picture is sent.
                camera = Camera.open(camera_id);
            } catch (Exception e) {
                return;
            }
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width,
                                   int height) {
            initPreview(width, height);
            startPreview();
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            // no-op
        }
    };

    Camera.PictureCallback photoCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            new SavePhotoTask().execute(data);
            camera.startPreview();
            inPreview = true;
            camera.release();
        }
    };



    class SavePhotoTask extends AsyncTask<byte[], String, String> {
        @Override
        protected String doInBackground(byte[]... jpeg) {


            boolean flash = settings.getBoolean("flash", true);



            String image_name = imageName();

            File dir = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "Swurv");
            if (!dir.exists()) {
                dir.mkdir();
            }

            File imagePath = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "Swurv" + File.separator + image_name);

            File photo = imagePath;

            if (photo.exists()) {
                photo.delete();
            }
            final String em = settings.getString("email", "");
            try {
                FileOutputStream fos = new FileOutputStream(photo.getPath());

                fos.write(jpeg[0]);
                fos.close();

                if(flash) {

                    SharedPreferences settings = getSharedPreferences("settings", Activity.MODE_PRIVATE);
                    int contrast = settings.getInt("progressChangeda", 1);
                    int brightness = settings.getInt("progressChangedb", 255);
                    String brightness1 = ("-" + brightness);

                    Bitmap old = BitmapFactory.decodeFile(imagePath
                            .getAbsolutePath());
                    Bitmap rotated = RotateBitmap(old, -90);
                    Bitmap ratated = changeBitmapContrastBrightness(rotated, contrast, brightness);
                    if (photo.exists()) {
                        photo.delete();
                    }

                    FileOutputStream fos1 = new FileOutputStream(photo);
                    ratated.compress(Bitmap.CompressFormat.PNG, 100, fos1);

                    fos1.flush();
                    fos1.close();
                }
                else {
                    Bitmap old = BitmapFactory.decodeFile(imagePath
                            .getAbsolutePath());
                    Bitmap ratated = RotateBitmap(old, -90);
                    if (photo.exists()) {
                        photo.delete();
                    }

                    FileOutputStream fos1 = new FileOutputStream(photo);
                    ratated.compress(Bitmap.CompressFormat.PNG, 100, fos1);

                    fos1.flush();
                    fos1.close();
                }



                try {
                    SendEmails emails = new SendEmails(getApplicationContext(),
                            "Someone tried to unlock your phone",
                            "Someone tried to unlock your phone");
                    emails.execute(em, imagePath.getAbsolutePath());
                    addImageToGallery(CaptureImageActivity.this,
                            imagePath.getAbsolutePath(), "Swurv", "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();
            } catch (java.io.IOException e) {
                Log.e("PictureDemo", "Exception in photoCallback", e);
            }

            return (null);
        }
    }

    public static Uri addImageToGallery(Context context, String filepath,
                                        String title, String description) {
        ContentValues values = new ContentValues();
        values.put(Media.TITLE, title);
        values.put(Media.DESCRIPTION, description);
        values.put(Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.MediaColumns.DATA, filepath);

        return context.getContentResolver().insert(Media.EXTERNAL_CONTENT_URI,
                values);
    }

    private String imageName() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        return timeStamp + ".jpg";
    }

    public static Bitmap RotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(),
                source.getHeight(), matrix, true);
    }

    /**
     *
     * @param bmp input bitmap
     * @param contrast 0..10 1 is default
     * @param brightness -255..255 0 is default
     * @return new bitmap
     */

    public static Bitmap changeBitmapContrastBrightness(Bitmap bmp, float contrast, float brightness)
    {
        ColorMatrix cm = new ColorMatrix(new float[]
                {
                        contrast, 0, 0, 0, brightness,
                        0, contrast, 0, 0, brightness,
                        0, 0, contrast, 0, brightness,
                        0, 0, 0, 1, 0
                });

        Bitmap ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());

        Canvas canvas = new Canvas(ret);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(cm));
        canvas.drawBitmap(bmp, 0, 0, paint);

        return ret;
    }
}