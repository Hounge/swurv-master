package com.swurv.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;



public class SelectUserActivity extends BaseActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_user);
	}

	public void NewUserClick(View view) {
		Intent i = new Intent(SelectUserActivity.this, SignUpActivity.class);
		startActivity(i);
//		finish();
	}

	public void ExistingUserClick(View view) {
		Intent i = new Intent(SelectUserActivity.this, LoginActivity.class);
		startActivity(i);
//		finish();
	}

}
