package com.swurv.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by hounge on 7/12/14.
 */
public class SuccessReceiver extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Intent i = new Intent(SuccessReceiver.this, Advertising.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(i);
    }
}