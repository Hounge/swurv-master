package com.swurv.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.Window;
import android.widget.TextView;

public class BaseActivity extends Activity {
	ProgressDialog m_pd = null;
	SharedPreferences settings;
	SharedPreferences.Editor editor;
	TextView txtBack;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Comment to show default bar. Uncomment to remove default bar
		//requestWindowFeature(Window.FEATURE_NO_TITLE);

		m_pd = new ProgressDialog(BaseActivity.this);
		// m_pd.setMessage("Please wait...");
		m_pd.setMessage(Html.fromHtml("Please wait..."));
		m_pd.setCancelable(false);
		
		settings = PreferenceManager.getDefaultSharedPreferences(this);
		editor = settings.edit();

	}


	public void showProgress() {
		if (m_pd != null) {
			m_pd.show();
		}
	}

	public void hideProgress() {
		if (m_pd != null) {
			m_pd.dismiss();
		}
	}
}